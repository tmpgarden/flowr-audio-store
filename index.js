var request = require('request');
var fs = require('fs');
var Hose = require('flowr-hose');
var fse = require('fs-extra');

var hose = new Hose({
  name: 'recstore',
  port: process.env.PORT || 3000,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

var audioStore = process.env.AUDIO_DIR;
if(audioStore.charAt(audioStore.length-1)!=='/'){
  audioStore = audioStore + '/';
}

var host = process.env.HOST;
if(host.charAt(host.length-1)!=='/'){
  host = host + '/';
}

hose.http.app.get('/*', function(req, res){
  var file = req.params['0'];
  console.log(file);
  res.sendFile(audioStore + file);
});

console.log("TTN.REPE is running...");


hose.process('add', process.env.CONCURRENCY || 1, function(job, done){
	errepikapenaSortu(job, function(err, data){
		console.log("errepikapena sortua!", data);
		done(err, data);
	});
});


function errepikapenaSortu(job, cb){
	job.log("repe:sortzen");
  var data = job.data;
  fse.ensureDirSync(audioStore + data.irratsaioa);

	var write = fs.createWriteStream(
    audioStore + data.irratsaioa + '/' + data.filename
  );

	request(data.url)
	.pipe(write);

	write.on('close', function(){
		job.log("repe:done");
    job.log({
      irratsaioa: data.irratsaioa,
      file: host + data.irratsaioa + '/' + data.filename
    });
		cb(null, {
      irratsaioa: data.irratsaioa,
      file: host + data.irratsaioa + '/' + data.filename
    });
	});
}
