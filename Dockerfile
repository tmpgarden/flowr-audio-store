FROM node:alpine
RUN apk add -U git
WORKDIR /opt/app
COPY index.js package.json /opt/app/
RUN npm install

ENTRYPOINT ["npm", "start"]
